import json

with open('students.json', 'r') as f:
    data = json.load(f)


def search_class_club(students, classname: str, club: str):
    student_list = []
    for st in students:
        if st["Class"] == classname and st["Club"] == club:
            student_list.append(st)
    return student_list


def filter_gender(students, gender: str):
    student_list = []
    for st in students:
        if st["Gender"] == gender:
            student_list.append(st)
    return student_list


def search_name(students, name: str):
    student_list = []
    for st in students:
        if st["Name"].find(name) != -1:
            student_list.append(st)
    return student_list


def get_name(students: list):
    name_list = []
    for st in students:
        for k, v in st.items():
            if k == "Name":
                name_list.append(v)
    return name_list


print(get_name(search_class_club(data, "3a", "Chess")))
print(get_name(filter_gender(data, "W")))
print(get_name(search_name(data, "ut")))
