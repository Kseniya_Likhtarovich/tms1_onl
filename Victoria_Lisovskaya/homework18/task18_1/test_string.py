import unittest
from main_string import counter


class TestLetterCount(unittest.TestCase):
    def test_one_letter(self):
        self.assertEqual(counter("a"), "a")

    def test_one_letter_several_time(self):
        self.assertEqual(counter("aaaa"), "a4")

    def test_few_letter(self):
        self.assertEqual(counter("abc"), "abc")

    def test_few_letter_several_times(self):
        self.assertEqual(counter("aaabbccccddddd"), "a3b2c4d5")

    def test_last_letters_several_times(self):
        self.assertEqual(counter("aabbbccdd"), "a2b3c2d2")

    def test_last_letters_one_time(self):
        self.assertEqual(counter("aabbbccd"), "a2b3c2d")

    def test_long_string(self):
        self.assertEqual(counter("aabbbccccdddddeeeffjhijklmnopqrstuvwxyz"),
                         "a2b3c4d5e3f2jhijklmnopqrstuvwxyz")

    @unittest.expectedFailure
    def test_negative_last_letter(self):
        self.assertEqual(counter("abcc"), "abc")

    @unittest.expectedFailure
    def test_negative_first_letter(self):
        self.assertEqual(counter("aabc"), "abc")


if __name__ == '__main__':
    unittest.main()
