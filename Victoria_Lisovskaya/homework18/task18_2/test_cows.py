import unittest
from main_cows import bulls_and_cows


class TestBullsCows(unittest.TestCase):

    def test_bulls_0_cows_0(self):
        self.assertEqual(bulls_and_cows("1234", "5678"),
                         "0 bull(s) and 0 cow(s)")

    def test_bulls_1_cows_0(self):
        self.assertEqual(bulls_and_cows("1234", "5278"),
                         "1 bull(s) and 0 cow(s)")

    def test_bulls_0_cows_1(self):
        self.assertEqual(bulls_and_cows("1234", "5628"),
                         "0 bull(s) and 1 cow(s)")

    def test_bulls_1_cows_1(self):
        self.assertEqual(bulls_and_cows("1234", "5624"),
                         "1 bull(s) and 1 cow(s)")

    def test_bulls_2_cows_0(self):
        self.assertEqual(bulls_and_cows("1234", "5634"),
                         "2 bull(s) and 0 cow(s)")

    def test_bulls_0_cows_3(self):
        self.assertEqual(bulls_and_cows("1234", "5423"),
                         "0 bull(s) and 3 cow(s)")

    def test_correct_answer(self):
        self.assertEqual(bulls_and_cows("1234", "1234"),
                         "You won! Congrats")

    @unittest.expectedFailure
    def test_incorrect_answer1(self):
        self.assertEqual(bulls_and_cows("1234", "1235"),
                         "You won! Congrats")

    @unittest.expectedFailure
    def test_incorrect_answer2(self):
        self.assertEqual(bulls_and_cows("1234", "1934"),
                         "2 bull(s) and 1 cow(s)")

    @unittest.expectedFailure
    def test_incorrect_answer3(self):
        self.assertEqual(bulls_and_cows("1234", "4321"),
                         "4 bull(s) and 0 cow(s)")

    @unittest.expectedFailure
    def test_incorrect_answer4(self):
        self.assertEqual(bulls_and_cows("1234", "9876"),
                         "0 bull(s) and 4 cow(s)")


if __name__ == "__main__":
    unittest.main()
