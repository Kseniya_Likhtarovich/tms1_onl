def test_second_button(browser):
    browser.get('https://ultimateqa.com/complicated-page/')
    browser.find_element_by_xpath(
        '//a[@class="et_pb_button et_pb_button_4 '
        'et_pb_bg_layout_light"]').click()
    browser.find_element_by_css_selector(
        '.et_pb_button_4.et_pb_bg_layout_light').click()
    browser.find_element_by_class_name('et_pb_button_4').click()
