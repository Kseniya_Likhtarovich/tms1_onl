import xml.etree.ElementTree as ET

root = ET.parse("library.xml").getroot()


def library(root, value, text):
    for child in root:
        for element in child:
            if element.tag == value and text in element.text:
                print(child.attrib["id"])


library(root, "author", "Shaw")
library(root, "price", "36.75")
library(root, "title", "Guide")
library(root, "description", "architect battles")
