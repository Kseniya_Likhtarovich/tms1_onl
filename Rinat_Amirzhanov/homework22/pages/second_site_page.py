import allure


class TestPage2Helper:
    def __init__(self, apps):
        self.apps = apps

    def open_home_page(self):
        driver = self.apps.driver
        with allure.step("Открыть главную страницу"):
            driver.get("http://demo.guru99.com/test/newtours/register.php")

    def set_firstname(self, firstname):
        driver = self.apps.driver
        with allure.step("Вводим имя"):
            firstname_input = driver.find_element_by_xpath(
                "//input[@name='firstName']")
            firstname_input.send_keys(firstname)

    def set_lastname(self, lastname):
        driver = self.apps.driver
        with allure.step("Вводим фамилию"):
            lastname_input = driver.find_element_by_xpath(
                "//input[@name='lastName']")
            lastname_input.send_keys(lastname)

    def set_phone(self, phone):
        driver = self.apps.driver
        with allure.step("Вводим номер телефона"):
            phone_input = driver.find_element_by_xpath(
                "//input[@name='phone']")
            phone_input.send_keys(phone)

    def set_email(self, email):
        driver = self.apps.driver
        with allure.step("Вводим почту"):
            email_input = driver.find_element_by_id("userName")
            email_input.send_keys(email)

    def set_address(self, adress):
        driver = self.apps.driver
        with allure.step("Вводим адрес"):
            address_input = driver.find_element_by_xpath(
                "//input[@name='address1']")
            address_input.send_keys(adress)

    def set_username(self, username):
        driver = self.apps.driver
        with allure.step("Вводим юзернейм"):
            username_input = driver.find_element_by_id("email")
            username_input.send_keys(username)

    def set_password(self, password):
        driver = self.apps.driver
        with allure.step("Вводим пароль"):
            password_input = driver.find_element_by_xpath(
                "//input[@name='password']")
            password_input.send_keys(password)

    def confirm_password(self, conf_password):
        driver = self.apps.driver
        with allure.step("Еще раз вводим пароль "
                         "для подтверждения"):
            conf_password_input = driver.find_element_by_xpath(
                "//input[@name='confirmPassword']")
            conf_password_input.send_keys(conf_password)

    def click_on_submit_button(self):
        driver = self.apps.driver
        with allure.step("Нажимаем на Confirm"):
            driver.find_element_by_xpath(
                "//input[@name='submit']").click()
