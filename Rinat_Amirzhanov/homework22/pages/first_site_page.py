import allure


class TestPage1Helper:
    def __init__(self, apps):
        self.apps = apps

    def open_home_page(self):
        driver = self.apps.driver
        with allure.step("Открыть главную страницу"):
            driver.get("http://thedemosite.co.uk/login.php")

    def set_username(self, username):
        driver = self.apps.driver
        with allure.step("Вводим логин"):
            search_input = driver.find_element_by_xpath(
                "//input[@name='username']")
            search_input.send_keys(username)

    def set_password(self, password):
        driver = self.apps.driver
        with allure.step("Вводим пароль"):
            search_input = driver.find_element_by_xpath(
                "//input[@name='password']")
            search_input.send_keys(password)

    def click_on_login(self):
        driver = self.apps.driver
        with allure.step("Нажимаем на кнопку Test Login"):
            search_input = driver.find_element_by_xpath(
                "//input[@name='FormsButton2']")
            search_input.click()
