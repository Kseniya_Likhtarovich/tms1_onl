import time

import allure


@allure.title("Тест для первой страницы")
def test_first_login(apps):
    apps.first_site.open_home_page()
    apps.first_site.set_firstname(firstname="fdgghgfhgf")
    apps.first_site.confirm_password(conf_password="dfgdfg")
    apps.first_site.click_on_login()
    time.sleep(1)
    assert apps.driver.find_element_by_xpath(
        "//td[@class='auto-style1']/big"
        "/blockquote/blockquote/font"
        "/center/b").text == "**Successful Login**"


@allure.title("Тест для второй страницы")
def test_second_login(apps, firstname="rinat", lastname="amirzhanov",
                      phone="77771245689", email="admin@mail.ru",
                      address="test", username="rinat",
                      password="admin", conf_password="admin"):
    apps.second_site.open_home_page()
    apps.second_site.set_firstname(firstname)
    apps.second_site.set_lastname(lastname)
    apps.second_site.set_phone(phone)
    apps.second_site.set_email(email)
    apps.second_site.set_address(address)
    apps.second_site.set_username(username)
    apps.second_site.set_password(password)
    apps.second_site.confirm_password(conf_password)
    apps.second_site.click_on_submit_button()
    time.sleep(2)
    assert apps.driver.find_element_by_xpath(
        "/html/body/div[2]/table/tbody/tr/td[2]"
        "/table/tbody/tr[4]/td/table/tbody/tr/td[2]"
        "/table/tbody/tr[3]/td/p[1]"
        "/font/b").text == f"Dear {firstname} {lastname},"
    assert apps.driver.find_element_by_xpath(
        "/html/body/div[2]/table/tbody/tr/td[2]"
        "/table/tbody/tr[4]/td/table/tbody/tr/td[2]"
        "/table/tbody/tr[3]/td/p[3]"
        "/font/b").text == f"Note: Your user name is {username}."
