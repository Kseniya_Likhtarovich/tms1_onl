def text(word):
    new_word = word[0]
    count = 0
    for i in range(len(word) - 1):
        if word[i] == word[i + 1]:
            count += 1
            if i == len(word) - 2:
                new_word += str(count + 1)
        else:
            if count > 0:
                new_word += str(count + 1)
                count = 0
            new_word += word[i + 1]
    return new_word
