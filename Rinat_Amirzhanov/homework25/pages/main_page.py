from Rinat_Amirzhanov.homework25.locators.main_page_locators\
    import MainPageLocators
from Rinat_Amirzhanov.homework25.pages.BasePage import BasePage


class MainPageHelper(BasePage):

    def should_be_main_page(self):
        best_sellers_text = self.find_element(
            MainPageLocators.LOCATOR_BEST_SELLERS_BUTTON).text
        assert best_sellers_text == "BEST SELLERS",\
            f'BEST SELLERS not eq {best_sellers_text}'

    def open_login_page(self):
        self.find_element(MainPageLocators.LOCATOR_SIGN_IN_BUTTON).click()

    def open_cart_page(self):
        self.find_element(MainPageLocators.LOCATOR_CART_PAGE).click()
