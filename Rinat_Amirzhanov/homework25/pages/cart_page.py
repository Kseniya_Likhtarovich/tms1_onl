from Rinat_Amirzhanov.homework25.locators.cart_page_locators\
    import CartPageLocators
from Rinat_Amirzhanov.homework25.pages.BasePage import BasePage


class CartPageHelper(BasePage):

    def should_be_cart_page(self):
        cart_text = self.find_element(
            CartPageLocators.LOCATOR_CART_PAGE_TEXT).text
        assert cart_text == "Your shopping cart"

    def check_cart_empty(self):
        cart_empty_text = self.find_element(
            CartPageLocators.LOCATOR_CART_EMPTY_TEXT).text
        assert cart_empty_text == "Your shopping cart is empty."
