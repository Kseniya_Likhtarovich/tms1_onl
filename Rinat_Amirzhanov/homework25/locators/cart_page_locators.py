from selenium.webdriver.common.by import By


class CartPageLocators:

    LOCATOR_CART_PAGE_TEXT = (
        By.XPATH, "//span[text()='Your shopping cart']")
    LOCATOR_CART_EMPTY_TEXT = (
        By.XPATH, "//p[text()='Your shopping cart is empty.']")
