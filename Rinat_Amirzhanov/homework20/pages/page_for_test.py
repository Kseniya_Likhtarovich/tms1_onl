import allure


class TestPageHelper:
    def __init__(self, app):
        self.app = app

    def open_home_page(self):
        driver = self.app.driver
        with allure.step("Открыть главную страницу"):
            driver.get("https://en.wikipedia.org/")

    def search_aricle(self):
        driver = self.app.driver
        with allure.step("Находим статью 'Python (programming language)'"):
            search_input = driver.find_element_by_id("searchInput")
            search_input.send_keys("Python (programming language)")
            search_input.submit()
