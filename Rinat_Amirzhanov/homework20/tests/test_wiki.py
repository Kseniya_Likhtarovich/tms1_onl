import allure


@allure.title("Тест для поиска статьи")
def test_simple_wiki(app):
    app.test_page.open_home_page()
    app.test_page.search_aricle()
    assert app.driver.find_element_by_id(
        "firstHeading").text == "Python (programming language)"
