from program import text
import pytest


@pytest.fixture(autouse=True)
def start_tests():
    print("Tests started")


@pytest.mark.parametrize("actual, expected", [
    ("abcde", "abcde"),
    ("cccba", "c3ba"),
    ("abdefffff", "abdef5"),
    ("abeehhhhhccced", "abe2h5c3ed"),
    ("aaabbceedd", "a3b2ce2d2"),
])
def test_word_positive(actual, expected):
    assert text(actual) == expected


@pytest.mark.xfail
def test_word_negative():
    assert text("cccba") == text("c2ba")
