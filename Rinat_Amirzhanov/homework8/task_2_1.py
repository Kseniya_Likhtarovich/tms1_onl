numbers = {0: 'zero', 1: 'one', 2: 'two', 3: 'three',
           4: 'four', 5: 'five', 6: 'six', 7: 'seven',
           8: 'eight', 9: 'nine', 10: 'ten', 11: 'eleven',
           12: 'twelve', 13: 'thirteen', 14: 'fourteen',
           15: 'fifteen', 16: 'sixteen', 17: 'seventeen',
           18: 'eighteen', 19: 'nineteen'}


def count(func):
    def decor(number_names: str):
        list_of_numbers = number_names.split(" ")
        words_numbers = []
        for k, v in numbers.items():
            for number in list_of_numbers:
                if number == str(k):
                    words_numbers.append(v)
        sorted_numbers = sorted(words_numbers)
        func(sorted_numbers)
    return decor


@count
def number(number_names):
    print(number_names)


number(number_names="1 2 3")
