import random


random_number = random.randint(0000, 9999)
random_number = str(random_number)
# Чтобы удостовериться, что все работает
print(random_number)
game = True
print("Давайте сыграем в игру Быки-коровы. Главная задача"
      " - отгадать число которое я загадаю. Поехали!")
while game:
    user_input = (input('Отгадайте загаданное 4-x значное число: '))
    # Делаем валидацию, чтобы пользователь ввел только 4-x значное число
    if len(user_input) > 4:
        print("4-x значное число!")
        continue
    elif len(user_input) < 4:
        print("4-x значное число!")
        continue

    list_random_number = list(random_number)
    list_user_input = list(user_input)
    cows = []
    bulls = []

    if random_number == user_input:
        print("Вы выйграли !")
        break
    if user_input[0] == random_number[0]:
        bulls.append(user_input[0])

    if user_input[1] == random_number[1]:
        bulls.append(user_input[1])

    if user_input[2] == random_number[2]:
        bulls.append(user_input[2])

    if user_input[3] == random_number[3]:
        bulls.append(user_input[3])
    for i in list_user_input:
        if i in list_random_number:
            cows.append(i)
    if len(cows) == 0:
        print("Ни одно из чисел не совпало. Попробуй еще раз")
        continue
    else:
        a = len(cows) - len(bulls)
        print(f'{a} коров, {len(bulls)} быков')
        continue
