# Positive numbers list

# Using generator
def num(num_list):
    for item in num_list:
        if item <= 0:
            continue
        else:
            yield item


for i in num([34.6, -203.4, 44.9, 68.3, -12.2, 44.6, 12.7]):
    print(i)
