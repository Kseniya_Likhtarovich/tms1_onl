# List of word length

# Using generator
def sentence(abc):
    for item in abc.split(" "):
        if item != "the":
            yield len(item)


for i in sentence("thequick brown fox jumps over the lazy dog"):
    print(i)
