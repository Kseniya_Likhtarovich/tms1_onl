from unittest import TestCase
from unittest import expectedFailure
from main_bnc import inp_check


class TestCases(TestCase):

    def test_correct(self):
        param = inp_check('8465')
        ret_value = True
        self.assertTrue(param, ret_value)

    def test_bulls_cows(self):
        param = inp_check('3264')
        ret_value = (1, 1)
        self.assertEqual(param, ret_value)

    def test_bulls(self):
        param = inp_check('3261')
        ret_value = (0, 1)
        self.assertEqual(param, ret_value)

    def test_bulls2(self):
        param = inp_check('3265')
        ret_value = (0, 2)
        self.assertEqual(param, ret_value)

    def test_bulls3(self):
        param = inp_check('3465')
        ret_value = (0, 3)
        self.assertEqual(param, ret_value)

    def test_cows(self):
        param = inp_check('3218')
        ret_value = (1, 0)
        self.assertEqual(param, ret_value)

    def test_cows2(self):
        param = inp_check('4218')
        ret_value = (2, 0)
        self.assertEqual(param, ret_value)

    def test_cows3(self):
        param = inp_check('4618')
        ret_value = (3, 0)
        self.assertEqual(param, ret_value)

    def test_cows4(self):
        param = inp_check('4658')
        ret_value = (4, 0)
        self.assertEqual(param, ret_value)

    @expectedFailure
    def test_duplicated_values(self):
        param = inp_check('4465')
        exp_value = False
        self.assertTrue(param, exp_value)

    @expectedFailure
    def test_too_long(self):
        param = inp_check('44651')
        exp_value = False
        self.assertTrue(param, exp_value)

    @expectedFailure
    def test_too_short(self):
        param = inp_check('4')
        exp_value = False
        self.assertTrue(param, exp_value)

    @expectedFailure
    def test_letters(self):
        param = inp_check('6aa1')
        exp_value = False
        self.assertTrue(param, exp_value)

    @expectedFailure
    def test_special_chars(self):
        param = inp_check('84$7')
        exp_value = False
        self.assertTrue(param, exp_value)

    @expectedFailure
    def test_empty(self):
        param = inp_check('')
        exp_value = False
        self.assertTrue(param, exp_value)
