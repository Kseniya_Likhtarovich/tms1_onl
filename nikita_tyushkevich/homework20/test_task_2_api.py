import wikipediaapi


def test_wiki_api():
    search = 'Python (programming language)'
    wiki_exemp = wikipediaapi.Wikipedia('en')
    search_page = wiki_exemp.page(search)
    assert search_page.exists(), f"{search} page doesn't exist"
    assert search_page.title == search, f"{search_page.title} is not {search}"
    print(f"Page title is {search_page.title}")
