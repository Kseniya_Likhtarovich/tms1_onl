from selenium.webdriver.common.by import By


class BasketPageLocators:

    LOCATOR_IS_BASKET_PAGE = (By.ID, "cart_title")
    LOCATOR_EMPTY_BASKET = (By.XPATH, '//p[@class="alert alert-warning"]')
