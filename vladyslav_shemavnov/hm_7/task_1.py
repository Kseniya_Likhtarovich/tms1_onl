# генераторы 1
num = [34.6, -203.4, 44.9, 68.3, -12.2, 44.6, 12.7]
positive_list = [i for i in num if i >= 0]
print(positive_list)


# через yield
def len_word(word: str):
    for item in word.split(" "):
        if item != "the":
            yield len(item)


for i in len_word("the quick brown fox "
                  "jumps over the lazy dog"):
    print(i)
