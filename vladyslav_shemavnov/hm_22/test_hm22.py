from selenium import webdriver
from selenium.webdriver.common.by import By
import time

browser = webdriver.Chrome()
browser.implicitly_wait(5)
browser.get("http://thedemosite.co.uk/login.php")


def test_check_success_login():
    username = browser.find_element(By.NAME, "username")
    username.send_keys("fdgghgfhgf")

    password = browser.find_element(By.NAME, "password")
    password.send_keys("dfgdfg")

    button = browser.find_element(By.NAME, "FormsButton2")
    button.click()

    time.sleep(2)
    xpath = "//b[contains(., '**Successful Login**')]"

    assert browser.find_element(By.XPATH, xpath).is_displayed()
