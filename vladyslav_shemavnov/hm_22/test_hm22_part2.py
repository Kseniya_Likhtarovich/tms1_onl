from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
import time

browser = webdriver.Chrome()
browser.implicitly_wait(5)
browser.get("http://demo.guru99.com/test/newtours/register.php")


def test_check_success_form():
    time.sleep(2)
    first_name = "Vlados"
    last_name = "Shemavnov"
    user_name = "vin diesel"
    fname = browser.find_element(By.NAME, "firstName")
    fname.send_keys(first_name)
    lname = browser.find_element(By.NAME, "lastName")
    lname.send_keys(last_name)
    phone = browser.find_element(By.NAME, "phone")
    phone.send_keys("0971629700")
    email = browser.find_element(By.NAME, "userName")
    email.send_keys("test@mail.com")
    address = browser.find_element(By.NAME, "address1")
    address.send_keys("Prolongacion Jesus 1428")
    city = browser.find_element(By.NAME, "city")
    city.send_keys("Guadalajara")
    state = browser.find_element(By.NAME, "state")
    state.send_keys("Jalisco")
    country = Select(browser.find_element_by_name('country'))
    country.select_by_value("MEXICO")
    username = browser.find_element(By.ID, "email")
    username.send_keys(user_name)
    password = browser.find_element(By.NAME, "password")
    password.send_keys("password")
    confirm_pass = browser.find_element(By.NAME, "confirmPassword")
    confirm_pass.send_keys("password")
    button = browser.find_element(By.NAME, "submit")
    button.click()

    res_1 = browser.find_element(By.XPATH, "//tr//table//tr//td//p[1]//b")
    res_2 = browser.find_element(By.XPATH, "//tr//table//tr//td//p[3]//b")

    assert f"{first_name} {last_name}" in res_1.text \
           and user_name in res_2.text
