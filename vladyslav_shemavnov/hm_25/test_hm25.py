from selenium import webdriver
import pytest


@pytest.fixture(scope="function")
def browser_hm_2():
    page = webdriver.Chrome()
    page.get("http://the-internet.herokuapp.com/frames")
    yield page
    page.quit()


def test_first(browser_hm_2):
    driver = browser_hm_2
    link_frame = driver.find_element_by_xpath("//a[contains(., 'iFrame')]")
    link_frame.click()
    fr = driver.find_element_by_id("mce_0_ifr")
    driver.switch_to.frame(fr)
    xpath = "//body[@id='tinymce']/p"
    assert driver.find_element_by_xpath(xpath).text == \
           "Your content goes here."
