import random
from collections import Counter

# переменные для игроков или опредеоения компьютера в игре
player_1 = None
player_2 = None
robot = False


print("Приветствую вас в игре Быки и коровы")
print("В игре доступно максимум 2 игрока "
      "или вы можете сыграть против Компьютера")
count_of_players = input("Введите кол-во игроков(максимум 2): ")
print()
print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
print()

# Алгоритм определения кол-ва игроков или участие компьютера
if int(count_of_players) == 1:
    player_1 = input("Игрок 1 введите своё имя: ")
    player_2 = "Robot"
    robot = True
else:
    player_1 = input("Игрок 1 введите своё имя: ")
    player_2 = input("Игрок 2 введите своё имя: ")

print()
print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
print()

# переменные для чисел игроков
number_of_player_1 = None
number_of_player_2 = None


# функция проверки числа на кол-во цифр и повторение цифр
def validation_for_number(number, player):
    good_number = 0
    while good_number != 1:
        count = Counter(number)
        # если длина числа не равна 4, тогда вводим число заново
        if len(number) == 4:
            for k in count.keys():
                # если появление цифры не равно 1, тогда вводим число заново
                if count[k] == 1:
                    good_number = 1
                else:
                    number = input(f"{player} вы ввели некорректное число, "
                                   f"попробуйте ещё раз: ")
                    good_number = 0
        else:
            number = input(f"{player} вы ввели некорректное число, "
                           f"попробуйте ещё раз: ")
            good_number = 0
    return number


# функция генерации числа для робота
def number_for_robot():
    number_try = random.sample(range(1, 10), 4)
    return ''.join(map(str, number_try))


if robot:
    # если игра с компьютером, тогда генерируем для него число
    number_of_player_2 = number_for_robot()
else:
    # если 2 пользователя, тогда спрашиваем их числа
    number_of_player_1 = input(f"{player_1} введите свое четырёхзначное число "
                               f"с неповторяющимися цифрами:")
    number_of_player_1 = validation_for_number(number_of_player_1, player_1)
    print(f"{player_1} вы ввели корректное число")
    # проверяем валидность числа
    number_of_player_2 = input(f"{player_2} введите свое четырёхзначное число "
                               f"с неповторяющимися цифрами:")
    # проверяем валидность числа
    number_of_player_2 = validation_for_number(number_of_player_2, player_2)
    print(f"{player_2} вы ввели корректное число")
print(number_of_player_1)
print(number_of_player_2)

print()
print("Отлично начнем игру")
print()

# переменные для определения загадывающего игрока и отгадывающего
answer_player = None
guessing_player = None
# переменная для определения загадываемого числа
number_for_game = None
# жребий
fate = random.randint(1, 2)

if robot:
    # если игра против компьютера, тогда он становится игроком 2
    # и загадывает число, игрок 1 отгадывает число
    answer_player = player_1
    guessing_player = player_2
    number_for_game = number_of_player_2
    print("Отгадывать будет игрок 1")
else:
    if fate == 1:
        # если жребий попадает на 1, тогда отгадывает игрок 1
        answer_player = player_1
        guessing_player = player_2
        number_for_game = number_of_player_2
        print("Отгадывать будет игрок 1")
    else:
        # если жребий попадает на 2, тогда отгадывает игрок 2
        answer_player = player_2
        guessing_player = player_1
        number_for_game = number_of_player_1
        print("Отгадывать будет игрок 2")


# Множеста с результатами успешных попыток отгадывания цифр в числах
cow_words = ("Одна Корова", "Две Коровы", "Три Коровы", "Четыре Коровы")
bull_words = ("Один Бык", "Два Быка", "Три Быка", "Четыре Быка")


# Сам мозг игры
def algorithm_for_game():
    # переменная для ответов отгадывающего игрока
    correct_answer = None
    # переменная для счетчика попыток
    count_of_try = 0
    # добавил подсказку, чтобы было проще проверять игру с компьютером
    print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    print(f"Не подсматривай, это подсказка {number_for_game}")
    print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")

    while correct_answer != number_for_game:
        # сисок для кол-ва отгаданных цифр в числе
        list_of_numbers = []
        # +1 к счетчику за попытку
        count_of_try += 1

        # переменная с ответом отгадывающего игрока
        correct_answer = input(f"{answer_player} "
                               f"твоя {count_of_try} попытка, вводи число: ")
        # проверка ответа на валидность числа
        correct_answer = validation_for_number(correct_answer, answer_player)
        # функционал проверки угаданных цифр со стороны компьютера
        if robot:
            # счетчик для правильных цифр без позиции
            count_n = 0
            # счетчик для правильных цифр с правильной позицией
            count_n_index = 0
            for i in correct_answer:
                # если цифра есть в правильном ответе
                if i in number_for_game:
                    # увеличиваю счетчик для правильных цифр без позиции
                    count_n += 1
                    # если индекс цифры
                    # которая есть в попытке отгадывающего игрока
                    # совпадает с индексом цифры в правильном ответе
                    if correct_answer.index(i) == number_for_game.index(i):
                        # уменьшаю счетчик для правильных цифр
                        # без позиции
                        count_n -= 1
                        # увеличиваю счетчик для правильных цифр
                        # с правильной позицией
                        count_n_index += 1
            # добавляю значения счетчиков правильных цифр в список
            list_of_numbers.append(count_n)
            list_of_numbers.append(count_n_index)
        else:
            # функционал записи ответов 2-го игрока,
            # сколько цифр угадал отгадывающий игрок
            approve_number = input(f"{guessing_player} сколько цифр "
                                   f"без позиции угадал отгадывающий игрок?: ")
            list_of_numbers.append(int(approve_number))
            approve_number_and_position = input(
                f"{guessing_player} сколько цифр с позицией "
                f"угадал отгадывающий игрок?: ")
            list_of_numbers.append(int(approve_number_and_position))

        # функционал вывода Коровы и Быки
        if correct_answer != number_for_game:
            # если счетчик правильных цифр без позиции и
            # счетчик для правильных цифр с правильной позицией больше 0
            if list_of_numbers[0] > 0 and list_of_numbers[1] > 0:
                # выводим результат в коровах и быках
                print(f"{cow_words[list_of_numbers[0] - 1]}, "
                      f"{bull_words[list_of_numbers[1] - 1]}")
            # если только счетчик правильных цифр без позиции больше
            elif list_of_numbers[0] > 0:
                # выводим результат в коровах
                print(cow_words[list_of_numbers[0] - 1])
            # если только счетчик для правильных цифр
            # с правильной позицией больше 0
            elif list_of_numbers[1] > 0:
                # выводим результат в коровах
                print(bull_words[list_of_numbers[1] - 1])
            # если пользователь ничего не угадал
            else:
                print("вы ничего не угадали")
            print(f"{answer_player} не унывай, попробуй ещё раз")
        else:
            print("ура ты победил!!!")


algorithm_for_game()
