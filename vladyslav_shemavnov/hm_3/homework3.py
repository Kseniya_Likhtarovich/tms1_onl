import random

# ex1
first_string = 'http://www.my_site.com/#about'
print(first_string.replace('#', '/'))


# ex2
print("Хотите добавить окончание ing к вашему слову? "
      "Тогда введите ваше слово:")
word = input()
word_end = "ing"
print(f"Ваше слово с окончанием ing: {word + word_end}")


# ex3
second_string = "Ivanou Ivan"
a = second_string.split(" ")
print(" ".join(list(reversed(a))))


# ex4
# easy option
third_string = " This text has a space at the beginning and at the end "
print(third_string[1:-1])


# funny option
# P.S Нужно было себя повеселить в пятничный вечер
list_with_answers = ["Ваша строка не начинается и не заканчивается "
                     "пробелом попробуйте ещё раз",
                     "Так сложно ввести строку которая начинается и "
                     "заканчивается на пробел?",
                     "Ты справишься просто не забудь поставить пробелы",
                     "Не переживай, у всех бывает впервые, просто намжми "
                     "space в начале и в конце",
                     "Мда, без бокала пива тут не обойтись! Попробуй снова"]

success_answer = 0
while success_answer != 1:
    print("Введите строку с пробелами в начале и в конце иначе я не отстану")
    ask_string = input()
    if ask_string.startswith(' ') and ask_string.endswith(' '):
        print(f"Ваша строка очищена от пробелов:{ask_string[1:-1]}")
        success_answer = 1
    else:
        print(">>> " + list_with_answers[random.randint(0, 4)])
