def words(word: str):
    res = word[0]
    count = 0
    for i in range(len(word) - 1):
        if word[i] == word[i + 1]:
            count += 1
        else:
            if count > 0:
                res += str(count + 1)
                count = 0
            res += word[i + 1]
    return res
