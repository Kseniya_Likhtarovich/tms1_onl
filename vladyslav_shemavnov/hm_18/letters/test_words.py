from letters_numbers import words
import unittest


class TestNumbersOfLetters(unittest.TestCase):

    def test_first_positive(self):
        self.assertTrue(words("aaabbvb"), "a3b2vb")

    def test_second_positive(self):
        self.assertTrue(words("abeehhhhhccced"), "abe2h5c3ed")

    def test_third_positive(self):
        self.assertTrue(words("44444"), "45")

    @unittest.expectedFailure
    def test_first_negative(self):
        self.assertFalse(words("aaabbvb"), "a5b3vb")
