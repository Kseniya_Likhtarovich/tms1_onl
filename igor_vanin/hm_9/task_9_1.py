class Book:
    def __init__(self, name, author, pages, ISBN, reserve=False, take=False):
        self.name = name
        self.author = author
        self.pages = pages
        self.ISBN = ISBN
        self.reserve = reserve
        self.take = take


class User:
    def __init__(self, name, books=None):
        if books is None:
            books = []
        self.reserve = True
        self.name = name
        self.list_bor_books = books  # Список книг доступных пользователю

    # Резрвируем книгу и проверяем флаг
    def reserved(self, book):
        if book.reserve is False:
            book.reserve = True
            print(f"Книга зарезервирована {self.name}!")
            self.list_bor_books.append(book.ISBN)
        else:
            print("Книга зарезервирована")

    # Возвращаем книгу которую взяли и резервировали
    def return_book(self, book):
        if book.ISBN in self.list_bor_books:
            book.reserve = False
            book.take = False
            self.list_bor_books.remove(book.ISBN)
            print(f"{self.name} вернули книгу!")
        else:
            print("Вами не зарезервирована данная книга!")

    # Берем книгу проверяем и переключаем флаг
    def take_book(self, book):
        if book.take is False:
            book.take = True
            print(f"Книгу взял {self.name}!")
            self.list_bor_books.append(book.ISBN)
        else:
            print("Книга недоступна")


book_1 = Book("Темная башня", "Стивен Кинг", "256", "9780340827222")
user_1 = User("Игорь")
user_2 = User("Дмитрий")

# Test
user_1.reserved(book_1)
user_2.reserved(book_1)
user_1.return_book(book_1)
user_2.reserved(book_1)
user_1.take_book(book_1)
user_2.take_book(book_1)
user_1.return_book(book_1)
user_2.take_book(book_1)
