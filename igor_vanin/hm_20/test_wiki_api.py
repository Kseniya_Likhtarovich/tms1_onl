import wikipediaapi


def test_wiki_api():
    result = 'Python_(programming_language)'
    wiki = wikipediaapi.Wikipedia('en')
    page_py = wiki.page('Python_(programming_language)')
    wiki_title = page_py.title
    assert wiki_title == result
