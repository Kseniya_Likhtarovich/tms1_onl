def counter(text):
    text_length = len(text)
    output = ""
    count = 1
    index = 0

    while index < text_length - 1:

        if text[index] == text[index + 1]:
            count += 1  # Обновляем count совпадением
        else:
            output += text[index]
            if count > 1:   # Не должно быть 1 внутри текста
                output += str(count)
            count = 1
        index += 1
    output += text[text_length - 1]
    if count > 1:   # В последнем симовле не должно быть 1
        output += str(count)

    return print(output)


counter("abeehhhhhccced")
counter("cccbba")
counter("abcde")
