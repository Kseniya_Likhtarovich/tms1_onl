import pytest
from bulls_and_cows_main import game


@pytest.fixture
def secret_number():
    secret_str = 3207
    return secret_str


@pytest.mark.smoke
class TestSmokeBullsAndCows:
    def test_win(self, secret_number):
        assert game(secret_number, 3207) == "Вы выйграли!"

    def test_smoke_return_bulls(self, secret_number):
        assert game(secret_number, 3217) == "0 коровы, 3 быка"

    def test_smoke_return_cows(self, secret_number):
        assert game(secret_number, 7123) == "3 коровы, 0 быка"

    def test_smoke_return_unity_bulls_cows(self, secret_number):
        assert game(secret_number, 3271), "1 коровы, 2 быка"


@pytest.mark.regression
class TestRegressionCases:
    def test_smoke_return_unity_bulls_cows(self, secret_number):
        assert game(secret_number, 32711), "Значение < 5 символов"

    def test_no_matches(self, secret_number):
        assert game(secret_number, 32711), "Нет совпадений"

    def test_string(self, secret_number):
        assert game(secret_number, "string"), "Нет совпадений"

    def test_repeat_number(self, secret_number):
        assert game(secret_number, 3322), "Нет совпадений"


class TestRevisionsAndBugs:
    def test_space_number(self, secret_number):
        with pytest.raises(IndexError):
            print("bug index error zero")
            assert game(secret_number, 0)

    @pytest.mark.xfail(reason="bug")
    def test_number_less_four(self, secret_number):
        with pytest.raises(IndexError):
            assert game(secret_number, 32)

    @pytest.mark.skip
    def test_number_less_null(self, secret_number):
        with pytest.raises(TypeError):
            assert game(secret_number, )
