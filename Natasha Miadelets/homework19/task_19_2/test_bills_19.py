from main_bills import bulls_and_cows
import pytest

number_1 = "3456"
number_2 = "3489"
number_3 = "3468"
number_4 = "5364"
number_5 = "1298"
number_6 = "9346"
answer_1 = "Вы выйграли!"
answer_2 = '0 коровы,', '2 быка'
answer_3 = '1 коровы,', '2 быка'
answer_4 = '4 коровы,', '0 быка'
answer_5 = '0 коровы,', '0 быка'
answer_6 = '2 коровы,', '1 быка'


@pytest.fixture
def start_test():
    print("Start test")


@pytest.mark.smoke
def test_bulls_4(start_test):
    assert bulls_and_cows(number_1) == answer_1


@pytest.mark.smoke
def test_cows_0_bulls_2(start_test):
    assert bulls_and_cows(number_2) == answer_2


@pytest.mark.smoke
@pytest.mark.parametrize('kind_of_animal', ["bills", "cows"])
def test_cows_1_bulls_2(start_test, kind_of_animal):
    assert bulls_and_cows(number_3) == answer_3


@pytest.mark.smoke
@pytest.mark.regression
def test_cows_4(start_test):
    assert bulls_and_cows(number_4) == answer_4


@pytest.mark.smoke
@pytest.mark.regression
def test_cows_0_bulls_0(start_test):
    assert bulls_and_cows(number_5) == answer_5


@pytest.mark.skip
def test_cows_2_bulls_1(start_test):
    assert bulls_and_cows(number_6) == answer_6


@pytest.mark.xfail(reason="bug 189")
def test_invalid_answer_1(start_test):
    assert bulls_and_cows(number_4) == answer_1


@pytest.mark.xfail(reason="bug 234")
def test_invalid_answer_2(start_test):
    assert bulls_and_cows(number_6) == answer_3
