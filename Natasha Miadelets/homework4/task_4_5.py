a = {'a': 1, 'b': 2, 'c': 3}
b = {'c': 3, 'd': 4, 'e': 5}
c = {**a, **b}
for v in c:
    if v in a and v in b:
        c[v] = [a[v], b[v]]
    elif v in a and v not in b:
        c[v] = [a[v], None]
    else:
        c[v] = [None, b[v]]
print(c)
