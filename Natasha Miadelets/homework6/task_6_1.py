card_number = list(input("Введите номер карты, "
                         "состоящий только из цифр без пробела:"))

simbols = "0123456789"
# Защита от дурака),тут надо бы побольше букв,
# но смысл думаю понятен


def validation_card_number():
    for iteam in card_number:
        if iteam not in simbols:
            print("Неверный формат введенного номера")
            exit()


validation_card_number()
# Разделение на два списка по индексам
list_multiplied = []
list_not_multiplied = []


def func_even():
    for i in range(len(card_number)):
        if i == 0 or i % 2 == 0:
            list_multiplied.append(card_number[i])
        else:
            list_not_multiplied.append(card_number[i])


def func_not_even():
    for i in range(len(card_number)):
        if i == 0 or i % 2 == 0:
            list_not_multiplied.append(card_number[i])
        else:
            list_multiplied.append(card_number[i])


def check_length_card_number():
    if len(card_number) % 2 == 0:
        func_even()
    else:
        func_not_even()


check_length_card_number()
# Умножаем на 2 каждое значение в list_multiplied и преобразуем в int
list_multiplied_int = [int(i) * 2 for i in list_multiplied]
list_first = []


# Для полученных значений > 9, отнимаем 9 и формируем новый список
def get_list_first():
    for k in list_multiplied_int:
        if k > 9:
            list_first.append(k - 9)
        else:
            list_first.append(k)


get_list_first()
# Преобразуем в int (здесь умножать не надо)
list_second = [int(i) for i in list_not_multiplied]

# Сумма общая для всего введенного кода
total_sum = sum(list_first) + sum(list_second)


# Проверка верности введенного кода и вывод результата
def get_result():
    # validation_card_number()
    if total_sum % 10 == 0:
        print("True")
    else:
        print("False")


# validation_card_number()
# check_length_card_number()
# get_list_first()
get_result()
