from pages.base_page import BasePage
from locators.Login_Page_Locators import LoginPageLocators


class LoginPage(BasePage):

    def should_be_login_page(self):
        create_account_text = self.find_element(
            LoginPageLocators.LOCATOR_CREATE_ACCOUNT_TEXT).text
        assert create_account_text == "CREATE AN ACCOUNT",\
            f'CREATE AN ACCOUNT not eq {create_account_text}'

    def login(self, email: str, passwd: str):
        email_field = self.find_element(LoginPageLocators.EMAIL_FIELD)
        email_field.send_keys(email)
        password = self.find_element(LoginPageLocators.PASSWORD_FIELD)
        password.send_keys(passwd)
        button = self.find_element(LoginPageLocators.SIGN_IN_BUTTON)
        button.click()

    def start_create_account(self, email: str):
        email_field = self.find_element(
            LoginPageLocators.LOCATOR_CREATE_ACCOUNT_FIELD)
        email_field.send_keys(email)
        create_account_button = self.find_element(
            LoginPageLocators.LOCATOR_CREATE_ACCOUNT_BUTTON)
        create_account_button.click()

    def check_create_account_form_is_open(self):
        register_button = self.find_element(
            LoginPageLocators.LOCATOR_REGISTER_BUTTON).text
        assert register_button == "Register",\
            f'Register not eq {register_button}'
