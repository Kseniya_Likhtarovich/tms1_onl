from selenium import webdriver
from time import sleep

url = "https://ultimateqa.com/filling-out-forms/"
browser = webdriver.Chrome("./chromedriver 2")
name = "Natasha"
message = "It is message"
name_field_xpath = "//input[@id=\"et_pb_contact_name_0\"]"
message_field_xpath = "//textarea[@id=\"et_pb_contact_message_0\"]"
submit_button_xpath = "//div[@class=\"et_contact_bottom_container\"]/button"
text_xpath = "//p[text()=\"Please refresh the page and try again.\"]"
browser.get(url)
# Заполняю поля
name_input = browser.find_element_by_xpath(name_field_xpath)
name_input.send_keys(name)
message_input = browser.find_element_by_xpath(message_field_xpath)
message_input.send_keys(message)
submit_button = browser.find_element_by_xpath(submit_button_xpath)
submit_button.click()

# Жду пока появится сообщение и ищу его текст
sleep(3)
text = browser.find_element_by_xpath(text_xpath).text
assert text == "Please refresh the page and try again.",\
    f'Please refresh the page and try again. is not equal {text}'
browser.quit()
