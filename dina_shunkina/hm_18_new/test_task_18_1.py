from dina_shunkina.hm_18.main1 import func
import unittest


class TestFunc(unittest.TestCase):
    def setUp(self):
        print("Сheck the error test")

    def tearDown(self):
        print("Сheck finished")

    def test_positive_1(self, setUp, tearDown):
        self.assertEqual(func("abeehhhhhccced"), "abe2h5c3ed")

    def test_positive_2(self, setUp, tearDown):
        self.assertEqual(func("123456"), "123456")

    @unittest.expectedFailure
    def test_negative_1(self, setUp, tearDown):
        self.assertEqual(func("abeehhhhh"), "abe2h5c3ed")

    def test_negative_2(self, setUp, tearDown):
        self.assertNotEqual(func("abeehhhhh"), "abe2h5c3ed")
