from dina_shunkina.hm_18.main2 import game
import unittest
from unittest import mock


class TestCasesForGame(unittest.TestCase):

    def setUp(self):
        print("Сhecks the error test")

    def tearDown(self):
        print("Сheck finished")

    def test_positive_1(self, setUp, tearDown):
        self.assertEqual(game("3219"), (1, 2))

    def test_positive_2(self, setUp, tearDown):
        m = mock.Mock()
        m.number = "1234"
        assert game(m.number), (4, 0)

    @unittest.expectedFailure
    def test_negative_1(self, setUp, tearDown):
        number_1 = "1#3#4"
        number_2 = False
        self.assertEqual(game(number_1), number_2)

    @unittest.skip("Bug OV-123")
    def test_negative_2(self, setUp, tearDown):
        number_1 = ""
        number_2 = False
        self.assertEqual(game(number_1), number_2)
