from time import sleep

from selenium import webdriver
from selenium.webdriver.support.select import Select

browser = webdriver.Chrome("./chromedriver")
browser.implicitly_wait(5)
browser.get("http://demo.guru99.com/test/newtours/register.php")

try:
    # Contact Information
    name_input = browser.find_element_by_css_selector("[name='firstName']")
    name_input.send_keys("Dina")
    last_name_input = browser.find_element_by_css_selector("[name='lastName']")
    last_name_input.send_keys("Shunkina")
    phone_input = browser.find_element_by_css_selector("[name='phone']")
    phone_input.send_keys("+375297865432")
    email_input = browser.find_element_by_id("userName")
    email_input.send_keys("dina@gmail.com")

    # Mailing Information
    address_input = browser.find_element_by_css_selector("[name='address1']")
    address_input.send_keys("7 Lewis Circle Ste. 313809")
    city_input = browser.find_element_by_css_selector("[name='city']")
    city_input.send_keys("Wilmington")
    state_input = browser.find_element_by_css_selector("[name='state']")
    state_input.send_keys("Delaware")
    postal_code = browser.find_element_by_css_selector("[name='postalCode']")
    postal_code.send_keys("19804")
    select = Select(browser.find_element_by_name("country"))
    select.select_by_visible_text("COLOMBIA")

    # User Information
    user_name = browser.find_element_by_id("email")
    user_name.send_keys("dina@gmail.com")
    password = browser.find_element_by_css_selector("[name='password']")
    password.send_keys("5wb7N6Q@zGkGzU6")
    confirm_password = browser.find_element_by_css_selector(
        "[name='confirmPassword']")
    confirm_password.send_keys("5wb7N6Q@zGkGzU6")
    button = browser.find_element_by_css_selector("[name='submit']")
    sleep(1)
    button.click()
    sleep(1)

    # Check Register
    find_first_last_name = browser.find_element_by_xpath(
        "//tr//table//font/b[contains(text(), 'Shunk')]")
    print(f" {find_first_last_name.text}")
    find_username = browser.find_element_by_xpath(
        "//tr//table//font/b[contains(text(), '@gmail.com')]")
    print(f"{find_username.text}")

finally:
    browser.quit()
