from selenium.webdriver.common.by import By


class MainPageLocator:
    SIGNIN_BUTTON = (By.CSS_SELECTOR, ".login")
    LOGO_TXT = (By.CSS_SELECTOR, ".logo.img-responsive")
    LOCATOR_CART_BUTTON = (By.XPATH, "//span[text()='(empty)']")
    LOCATOR_SEARCH_FIELD = (By.CSS_SELECTOR, "#search_query_top")
    LOCATOR_SEARCH_BUTTON = (By.CSS_SELECTOR, "[name='submit_search']")
