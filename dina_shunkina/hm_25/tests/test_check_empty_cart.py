from dina_shunkina.hm_25.pages.main_page import MainPage


def test_user_search_item(browser):
    main_page = MainPage(browser)
    main_page.open_page()
    main_page.should_be_signin_page()
    main_page.check_empty_cart()
