from dina_shunkina.hm_25.pages.base_page import BasePage
from dina_shunkina.hm_25.locators.search_page_locator import SearchPageLocator


class SearchPage(BasePage):
    def should_be_search_page(self):
        self.visibility_element(SearchPageLocator.LOCATOR_SEARCH_TEXT)
