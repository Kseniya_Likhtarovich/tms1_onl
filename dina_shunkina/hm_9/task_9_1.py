class Book:

    def __init__(self, name_book, author, pages, isbn,
                 flag, available, reserved):
        self.name_book = name_book
        self.author = author
        self.pages = pages
        self.isbn = isbn
        self.flag = flag
        self.available = available
        self.reserved = reserved


class User:
    def __init__(self, name_user, available_book=None):
        if available_book is None:
            available_book = []
        self.name_user = name_user
        self.available_book = available_book

    def method_get_book(self, book):
        if book.available is False:
            print(f" {self.name_user} взял(a) книгу {book.name_book}")
            book.available = True
            self.available_book.append(book.name_book)
        else:
            print(f"Книга {book.name_book} отсуствует")

    def method_return_book(self, book):
        if book.name_book in self.available_book:
            self.available_book.remove(book.name_book)
            book.available = False
            print(f"{self.name_user} вернул(a) книгу {book.name_book}")
        else:
            print(f"Пользователь не брал книгу {book.name_book}")

    def method_reserve_book(self, book):
        if book.reserved is False and book.available is False:
            print(f"{self.name_user} забранировал(a) книгу {book.name_book}")
            book.reserved = True
        elif book.reserved or book.available:
            print(f"Книга {book.name_book} забранирована")


book_available_ed1 = Book("A Byte of Python",
                          "Swaroop C H",
                          164,
                          9781977878496,
                          "eng", False, False)

book_available_ed2 = Book("A Byte of Python - part 2",
                          "Swaroop C H",
                          188,
                          9781929878412,
                          "eng", False, False)

name1 = User("Дина")
name2 = User("Дима")

name1.method_return_book(book_available_ed1)
name2.method_reserve_book(book_available_ed1)
name2.method_get_book(book_available_ed1)
name2.method_return_book(book_available_ed1)

name1.method_get_book(book_available_ed1)
name2.method_get_book(book_available_ed1)

name1.method_reserve_book(book_available_ed2)
name2.method_reserve_book(book_available_ed1)

name1.method_return_book(book_available_ed1)
name2.method_return_book(book_available_ed2)
