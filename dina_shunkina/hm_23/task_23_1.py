from selenium import webdriver

browser = webdriver.Chrome("./chromedriver")
browser.implicitly_wait(5)
browser.get("https://ultimateqa.com/complicated-page/")

try:
    # 1)XPATH
    button_XPATH = browser.find_element_by_xpath(
        "//a[@class = 'et_pb_button et_pb_button_4 et_pb_bg_layout_light']")
    button_XPATH.click()
    # 1)CSS selector
    button_CSS_selector = browser.find_element_by_css_selector(
        ".et_pb_button.et_pb_button_4.et_pb_bg_layout_light")
    button_CSS_selector.click()
    # 1)class name
    button_class_name = browser.find_element_by_class_name(
        "et_pb_button.et_pb_button_4.et_pb_bg_layout_light")
    button_class_name.click()
finally:
    browser.quit()
