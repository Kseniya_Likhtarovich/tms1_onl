from selenium import webdriver

browser = webdriver.Chrome("./chromedriver")
browser.implicitly_wait(5)
browser.get("http://the-internet.herokuapp.com/frames")

text = "Your content goes here."
iFrame_link = browser.find_element_by_xpath("//a[text()='iFrame']").click()
switch_to_frame = browser.switch_to.frame(
    browser.find_element_by_css_selector("#mce_0_ifr"))
find_text = browser.find_element_by_css_selector("#tinymce >p").text
assert find_text == text, f"is found text {find_text} is not eq {text}"

browser.quit()
