import wikipediaapi


def test_wiki_page_api():
    word = "Programming language"
    en_wiki_page = wikipediaapi.Wikipedia('en')
    page_en = en_wiki_page.page(word)
    assert page_en.exists(), f"{word} doesn't exists"
    assert page_en.title == word, f"{page_en.title} is not eq {word}"
    print(f"Current title is {page_en.title}")
    print(f"Current page:\n {page_en.text}")
