class Investment:
    def __init__(self, surname, amount, time, result_amount=0):
        self.name = surname
        self.amount = amount
        self.time = time
        self.result_amount = result_amount


class Bank:
    def __init__(self, name, deposit_rate):
        self.name = name
        self.deposit_rate = deposit_rate

# расчет суммы по вкладу с ежемесячной капитализацией
    def deposit(self, n: int, r: int):
        k = self.deposit_rate / 100
        month = r * 12
        result = n * pow((1 + k / 12), month)
        return result


invest1 = Investment("Petrov", 1000, 3)
bank1 = Bank("Prior", 10)
bank2 = Bank("MTBank", 15)
invest1.result_amount = bank1.deposit(invest1.amount, invest1.time)
print(invest1.result_amount)
invest1.result_amount = bank2.deposit(invest1.amount, invest1.time)
print(invest1.result_amount)
