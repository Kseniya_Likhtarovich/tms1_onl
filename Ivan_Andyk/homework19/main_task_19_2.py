def game(pc_number, answer):

    bulls = []
    cows = []
    if answer == pc_number:
        return "Вы выйграли"

    else:
        for i in answer:
            if i in pc_number:
                if pc_number.index(i) == answer.index(i):
                    cows.append(i)
                else:
                    bulls.append(i)

        return "Быки: {}, Коровы: {}".format(len(cows), len(bulls))
