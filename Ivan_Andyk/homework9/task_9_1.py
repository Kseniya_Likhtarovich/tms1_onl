class Book:
    def __init__(self, book_name, author, number_page,
                 isbn, take, reserved):
        self.book_name = book_name
        self.author = author
        self.number_page = number_page
        self.isbn = isbn
        self.take = take
        self.reserved = reserved


class User:
    def __init__(self, user_name, book=None):
        if book is None:
            book = []
        self.user_name = user_name
        self.book = book

    def book_take(self, book):
        if book.take is False:
            print(self.user_name, f"вы можете взять {book.book_name}")
            book.take = True
            self.book.append(book.isbn)
        else:
            print(self.user_name, f"книга {book.book_name} не доступна")

    def book_return(self, book):
        if book.isbn in self.book:
            self.book.remove(book.isbn)
            book.isbn = False
            print(self.user_name, f"вернул книгу {book.book_name}")
        else:
            print(self.user_name, f"книга {book.book_name} доступна")

    def book_reserved(self, book):
        if book.take is False:
            print(self.user_name, f"вы забронировали {book.book_name}")
            book.take = True
            self.book.append(book.isbn)
        else:
            print(self.user_name, f"книга {book.book_name} не доступна")


book1 = Book("На ростанях", "Я. Колас", "600", "3294792427", False, False)
book2 = Book("Мастер и Маргарита", "М. Булгаков", "500", "6594794527",
             False, False)
user1 = User("Иван")
user2 = User("Дмитрий")

user1.book_take(book1)
user2.book_take(book1)
user1.book_return(book1)
user2.book_return(book1)
user1.book_reserved(book2)
user2.book_reserved(book2)
