numbers = [34.6, -203.4, 44.9, 68.3, -12.2, 44.6, 12.7]


def gen(num):
    for i in num:
        if i <= 0:
            continue
        else:
            yield i


gen(numbers)
print([i for i in gen(numbers)])
