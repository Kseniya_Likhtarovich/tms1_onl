from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

url = "https://www.wikipedia.org/"

driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver")

driver.get(url)
search_phrase = "Programming language"
search = driver.find_element_by_css_selector("#searchInput")
search.send_keys(search_phrase)
search.send_keys(Keys.RETURN)
time.sleep(3)
result_page_title = driver.find_element_by_css_selector(".firstHeading").text
assert search_phrase == result_page_title

driver.close()
