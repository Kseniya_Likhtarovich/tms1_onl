import wikipediaapi


def test_wiki_api():
    test_phrase = "Programming language"
    en_wiki_page = wikipediaapi.Wikipedia('en')
    page_en = en_wiki_page.page(test_phrase)
    assert page_en.exists(), f"{test_phrase} page doesn't exists"
    assert page_en.title == test_phrase, \
        f"{page_en.title} is not equal {test_phrase}"
