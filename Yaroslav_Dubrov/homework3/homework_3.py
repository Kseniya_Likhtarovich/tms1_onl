# Task 1
test_str = "www.my_site.com#about"
new_str = test_str.replace("#", "/")

print("Task 1 result:", new_str)

# Task 2
my_word = "say"
result_word = my_word + "ing"

print("Task 2 result:", result_word)

# Task 3
msg_1 = "Ivanou Ivan"
list_1 = msg_1.split()
list_2 = [list_1[1], list_1[0]]
msg_2 = " ".join(list_2)

print("Task 3 result:", msg_2)

# Task 4
str_spaces = " Hello World "
l_sp_result = str_spaces.lstrip()
r_sp_result = str_spaces.rstrip()

print("Task 4 results below:", l_sp_result, r_sp_result, sep="\n")
