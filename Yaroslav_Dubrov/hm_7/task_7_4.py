# The Caesar Cipher with Bonus tasks 1,2,3
user_str = input("Enter your phrase: ")
user_shift = int(input("Enter shift value: "))

# We can add to alphabet any symbols if we need
alphabet = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz" + \
    "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ" + \
    "абвгдеёжзийклмнопрстуфхцчшщъыьэюя" + \
    "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"


# Here we ask user what he want to do and run next function
def precondition():
    choice = int(input("You want to: 1 - Encode 2 - Decode? "))
    if choice == 1:
        print(encode_decode(user_str, user_shift, choice))
    elif choice == 2:
        print(encode_decode(user_str, user_shift, choice))


# Main working function
def encode_decode(user_str, user_shift, choice):
    result = ""

    for i in user_str:
        # Here we found each element from user_str in alphabet
        mesto = alphabet.find(i)

        # This is Encoding
        if choice == 1:

            # Find symbol for replacement
            new_mesto = mesto + user_shift
            if i in alphabet:
                result += alphabet[new_mesto]
            else:
                result += i

        # This is Decoding
        elif choice == 2:

            # Find symbol for replacement
            new_mesto = mesto - user_shift
            if i in alphabet:
                result += alphabet[new_mesto]
            else:
                result += i

    return result


precondition()
