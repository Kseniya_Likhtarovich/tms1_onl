from strings_w_symbol import count_letters
import unittest


class TestStrings(unittest.TestCase):
    def test_positive_one(self):
        self.assertEqual(count_letters("aaabbc"), "a3b2c")

    def test_positive_two(self):
        self.assertEqual(count_letters("xccfffggh"), "xc2f3g2h")

    def test_positive_three(self):
        self.assertEqual(count_letters("aabaaa"), "a2ba3")

    @unittest.expectedFailure
    def test_negative_cyrrilic_a(self):
        self.assertEqual(count_letters("аааbbb"), "a3b3")

    @unittest.expectedFailure
    def test_negative_numbers_in_phrase(self):
        self.assertEqual(count_letters("aaa33dd"), "a3d2")

    @unittest.expectedFailure
    def test_negative_space_in_phrase(self):
        self.assertEqual(count_letters("   "), " 3")
