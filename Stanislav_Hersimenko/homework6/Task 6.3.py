print("Выберете операцию: 1. Сложение, 2. Вычитание, 3. Умножение, 4. Деление")
# Для отмены операции, ноль нужно вводить в каждое поле ввода.
print("Для отмены введите 0")


def calculator():
    if math_symbol == "1":
        result1 = Number1 + Number2
        print(result1)
    elif math_symbol == "2":
        result2 = Number1 - Number2
        print(result2)
    elif math_symbol == "3":
        result3 = Number1 * Number2
        print(result3)
    elif math_symbol == "4":
        if Number2 != 0:
            result4 = Number1 / Number2
            print(result4)
        else:
            print("На ноль не делится")


numbers_inputted = True
while numbers_inputted:
    math_symbol = input("Выберете операцию: ")
    Number1 = int(input())
    Number2 = int(input())
    if math_symbol == "0":
        print("Отмена")
        numbers_inputted = False
    else:
        calculator()
