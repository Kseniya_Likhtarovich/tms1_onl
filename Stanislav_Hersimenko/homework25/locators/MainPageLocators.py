from selenium.webdriver.common.by import By


class MainPageLocators:
    LOCATOR_SING_IN_BUTTON = (By.CLASS_NAME, "login")
    LOCATOR_CART_LINK = (By.CSS_SELECTOR, "a[title='View my shopping cart']")
    LOCATOR_T_SHIRTS = (By.XPATH, '//*[@id="block_top_menu"]/ul/li[3]/a')
    LOCATOR_T_SHIRTS_ADD = (
        By.XPATH, '//*[@id="center_column"]/ul/li/div/div[2]/div[2]/a[1]/span')
    LOCATOR_CHECKOUT = (
        By.XPATH, '//*[@id="layer_cart"]/div[1]/div[2]/div[4]/a/span/i')
