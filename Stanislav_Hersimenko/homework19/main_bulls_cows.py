is_playing = True


def bulls_cows(secret_number, player_input):
    amount_bulls = 0
    amount_cows = 0
    splitted_input = [int(i) for i in str(secret_number)]
    splitted_player_input = [int(i) for i in str(player_input)]
    for index, elem in enumerate(splitted_input):
        player_elem = splitted_player_input[index]
        if elem == player_elem:
            amount_bulls = amount_bulls + 1
        elif elem in splitted_player_input:
            amount_cows = amount_cows + 1
    if amount_bulls == 4:
        print("Вы выиграли!")
        global is_playing
        is_playing = False
    else:
        print("Быки: " + str(amount_bulls), "Коровы: " + str(amount_cows))
    return [amount_bulls, amount_cows]


while is_playing:
    secret_number = 4569
    player_input = input("Введите ваше число: ")
    bulls_cows(secret_number, player_input)
