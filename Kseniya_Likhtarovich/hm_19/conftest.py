import pytest


def pytest_make_parametrize_id(val):
    return repr(val)


@pytest.fixture(scope="class", autouse=True)
def start_end():
    print("Начать тестирование.")
    yield
    print("Тестирование завершено.")


@pytest.fixture
def message():
    error_message = "Нужно ввести 4 НЕПОВТОРЯЮЩИЕСЯ ЦИФРЫ!"
    return error_message
