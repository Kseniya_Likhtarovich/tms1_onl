"""
На вход подается строка. Нужно посчитать количество одинаковых букв.

Например, "cccbba" -> строка “c3b2a".
"""


def word(chr):
    """Подсчитывает количество букв в строке."""
    chr_new = chr + " "
    ctr, chr_ind = 0, chr_new[0]
    for i in chr_new:
        if chr_ind != i:
            print(chr_ind + str(ctr).replace("1", ""), end="")
            ctr, chr_ind = 0, i
        ctr += 1


word("aaabbvb")
