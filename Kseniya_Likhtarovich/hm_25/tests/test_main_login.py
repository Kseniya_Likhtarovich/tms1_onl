from Kseniya_Likhtarovich.hm_25.pages.main_page import MainPage
from Kseniya_Likhtarovich.hm_25.pages.login_page import LoginPage


def test_login(browser):
    main_page = MainPage(browser)
    main_page.open_main_page()
    main_page.open_login_page()

    login_page = LoginPage(browser)
    login_page.should_be_login_page()
    login_page.login_error()
    login_page.login("casey_li@test.com", "12345")
