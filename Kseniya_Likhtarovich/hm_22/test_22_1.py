"""Залогинется созданным пользователем."""


def test_user(browser):
    browser.get("http://thedemosite.co.uk/addauser.php")

    user = browser.find_element_by_xpath("//input[@name='username']")
    user.send_keys("Kseniya")

    password = browser.find_element_by_xpath("//input[@name='password']")
    password.send_keys("Qwerty12")

    button = browser.find_element_by_xpath("//input[@type='button']")
    button.click()


def test_login(browser):
    browser.get("http://thedemosite.co.uk/login.php")

    user = browser.find_element_by_xpath("//input[@name='username']")
    user.send_keys("Kseniya")

    password = browser.find_element_by_xpath("//input[@name='password']")
    password.send_keys("Qwerty12")

    button = browser.find_element_by_xpath("//input[@type='button']")
    button.click()

    msg = browser.find_element_by_xpath("//center/b")
    assert msg.text == "**Successful Login**"
