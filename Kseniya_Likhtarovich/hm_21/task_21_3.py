"""
Работа с json файлом.

Разработать поиск учащихся в одном классе, посещающих одну секцию.
Фильтрация учащихся по их полу. Поиск ученика по имени(часть имени).
"""

import json


class Students:

    def __init__(self, json_file):
        with open(json_file, "r") as file:
            self.data = json.load(file)

    def search_class_club(self, form, club):
        """Поиск учащихся в одном классе, посещающих одну секцию."""
        for i in self.data:
            if i["Class"] == form and i["Club"] == club:
                yield i

    def filter_gender(self, gender):
        """Фильтрация учащихся по их полу."""
        for i in self.data:
            if i["Gender"] == gender:
                yield i

    def search_name(self, name):
        """Поиск ученика по имени(часть имени)."""
        for i in self.data:
            if name in i["Name"]:
                return i


students = Students("students.json")

print("Поиск учащихся в одном классе, посещающих одну секцию:")
search1 = students.search_class_club("5a", "Football")
for j in search1:
    print(j)

print("Поиск учащихся по полу:")
search2 = students.filter_gender("W")
for j in search2:
    print(j)

print("Поиск учащихся по имени:")
print(students.search_name("Yuna"))
