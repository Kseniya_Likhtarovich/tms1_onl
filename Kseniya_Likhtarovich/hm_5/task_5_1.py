"""Реализовать программу, против которой можно сыграть в "Быки и коровы"."""

import random

bulls, cows, game = 0, 0, True

while game:
    player1 = list(int(i) for i in input("Введите 4 неповторяющиеся цифры: "))
    if len(player1) != 4 or len(player1) != len(set(player1)):
        print("4 НЕПОВТОРЯЮЩИЕСЯ ЦИФРЫ!")
        continue

    player2 = random.sample(range(10), k=4)
    print(player2)

    for i in zip(player1, player2):
        if i[0] == i[1]:
            bulls += 1
        elif i[0] in player2:
            cows += 1
    else:
        if player1 == player2:
            print(f"Вы победитель по жизни! Быки: {bulls}, Коровы: {cows}")
            break
    print(f"Быки: {bulls}, Коровы: {cows}. Поздравляем!!! Ещё раз...")
    bulls, cows = 0, 0
