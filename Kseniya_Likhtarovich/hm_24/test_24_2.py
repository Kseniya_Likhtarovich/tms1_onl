from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def test_iframe(browser):
    browser.get("http://the-internet.herokuapp.com/frames")

    link = browser.find_element_by_xpath("//a[text()='iFrame']")
    link.click()

    WebDriverWait(browser, 10).until(
        EC.frame_to_be_available_and_switch_to_it((By.XPATH, "//iframe")))

    msg = browser.find_element_by_id("tinymce").text
    assert msg == "Your content goes here."
