from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def test_dynamic_control(browser):
    checkbox_xpath = "//*[@id='checkbox']/input"
    browser.find_element(By.XPATH, checkbox_xpath)

    button_xpath = "//form[@id='checkbox-example']/button"
    button = browser.find_element(By.XPATH, button_xpath)
    button.click()

    WebDriverWait(browser, 10).until(
        EC.visibility_of_element_located((By.ID, "message")))
    WebDriverWait(browser, 10).until(
        EC.invisibility_of_element((By.XPATH, checkbox_xpath)))

    input_xpath = "//*[@id='input-example']/input"
    input = browser.find_element(By.XPATH, input_xpath)

    assert input.get_attribute("disabled")

    button2_xpath = "//*[@id='input-example']/button"
    button2 = browser.find_element(By.XPATH, button2_xpath)
    button2.click()

    WebDriverWait(browser, 10).until(
        EC.visibility_of_element_located((By.ID, "message")))

    assert input.is_enabled()
