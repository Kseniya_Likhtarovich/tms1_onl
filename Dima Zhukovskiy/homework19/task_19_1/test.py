from main import bulls
import pytest

number_1 = "1413"
number_2 = "4444"
number_3 = "1314"
number_4 = "1111"
number_5 = "1212"
number_6 = "3333"
answer_1 = "Вы выиграли!"
answer_2 = '3 Коровы', '1 Быки'
answer_3 = '2 Коровы', '2 Быки'
answer_4 = '2 Коровы', '2 Быки'
answer_5 = '0 Коровы', '2 Быки'
answer_6 = '3 Коровы', '1 Быки'


@pytest.fixture
def start_test():
    print("Start test")


@pytest.mark.smoke
def test_bulls_4(start_test):
    assert bulls(number_1) == answer_1


@pytest.mark.smoke
def test_cows_0_bulls_2(start_test):
    assert bulls(number_2) == answer_2


@pytest.mark.smoke
@pytest.mark.parametrize('kind_of_animal', ["bills", "cows"])
def test_cows_1_bulls_2(start_test, kind_of_animal):
    assert bulls(number_3) == answer_3


@pytest.mark.smoke
@pytest.mark.regression
def test_cows_4(start_test):
    assert bulls(number_4) == answer_4


@pytest.mark.smoke
@pytest.mark.regression
def test_cows_0_bulls_0(start_test):
    assert bulls(number_5) == answer_5


@pytest.mark.skip
def test_cows_2_bulls_1(start_test):
    assert bulls(number_6) == answer_6


@pytest.mark.xfail(reason="bug 189")
def test_invalid_answer_1(start_test):
    assert bulls(number_4) == answer_1


@pytest.mark.xfail(reason="bug 234")
def test_invalid_answer_2(start_test):
    assert bulls(number_6) == answer_3
