a = {'a': 1, 'b': 2, 'c': 3}
b = {'c': 3, 'd': 4, 'e': 5}
c = {k: [a.get(k, None), b.get(k, None)] for k in sorted(set(a) | set(b))}
print(c)
